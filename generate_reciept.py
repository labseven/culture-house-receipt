
import sys
import os
from PIL import Image, ImageFont, ImageDraw
import tempfile
import textwrap


def wrapText(text, lineWidth=48):
    return '\n'.join([textwrap.fill(line, lineWidth) for line in text.splitlines()])


def generate_image(in_text):
    margins = (20, 50)
    extra_white = 130

    # 30 chars per line with this font
    font = ImageFont.truetype("DINOT.otf", size=30)
    text = wrapText(in_text, lineWidth=30)

    # Find the size of the text
    text_size = ImageDraw.Draw(Image.new("RGB",(10,10))).multiline_textsize(text, font)

    canvas_size = (500, text_size[1] + margins[1]*2 + extra_white)
    img = Image.new("RGB", canvas_size, color="white")


    d = ImageDraw.Draw(img)

    # print(canvas_size, margins, text_size)
    position = (margins[0], margins[1] + extra_white)

    d.multiline_text(position, text, fill="black", font=font)

    img = img.rotate(180)

    file_name = tempfile.NamedTemporaryFile().name
    img.save(file_name, "PNG")

    # os.system("eom " + file_name)

    return file_name


if __name__ == "__main__":
    print(generate_image(sys.argv[1]))

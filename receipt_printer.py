import textwrap
import subprocess

print_to = "/dev/usb/lp0" # none, adafruit, serial


def wrapText(text, lineWidth=48):
    return '\n'.join([textwrap.fill(line, lineWidth) for line in text.splitlines() if line != "[FROM AN EXTERNAL SENDER]"])


def receiptPrintText(text):
    print("printing...")

    pre_pad = 4 if len(text.split('\n')) < 5 else 1
    post_pad = 12 if len(text.split('\n')) < 12 else 4

    text = wrapText(text)

    print(text)
    try:
        with open(print_to, 'w') as printer:
            printer.write('\n'*pre_pad)
            printer.write(text)
            printer.write('\n'*post_pad)
    except FileNotFoundError:
        print("can't connect to printer")

def spoolPaper(num_lines):
    try:
        with open(print_to, 'w') as printer:
            printer.write("\n" + '*'*53 + "\n")
            printer.write('\n'*num_lines)
    except FileNotFoundError:
        print("can't connect to printer")

def printImage(filename):
    subprocess.run(["lpr", "-o orientation-requested=3", "-o fit-to-page", filename])

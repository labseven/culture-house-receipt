# /usr/bin/env python

from flask import Flask, request
from twilio.twiml.messaging_response import MessagingResponse
import receipt_printer
import time 
import sqlite3
import generate_reciept


app = Flask(__name__)


def get_sms_info(request):
    return {
        "body": str(request.form.get("Body")),
        "from": str(request.form.get("From")),
        "timestamp": time.time()
    }

def sql_time(time_val):
    return time.strftime("%Y-%m-%d %H:%M:%S",time.localtime(time_val))

def human_time(time_val):
    return time.strftime("%a %d   (%H:%M)",time.localtime(time_val))


def save_sms(sms):
    with sqlite3.connect("sms_info.db") as conn:
        c = conn.cursor()
        sql = "insert into sms (sms_from, sms_body, timestamp) values (?, ?, ?)"
        c.execute(sql, (sms["from"], sms["body"], sql_time(sms["timestamp"])))
        conn.commit()

def find_by_number(phonenumber):
    with sqlite3.connect("sms_info.db") as conn:
        c = conn.cursor()
        c.execute("select count() from sms where sms_from = ?", (phonenumber,))
        return c.fetchone()

@app.route("/", methods=['GET'])
def home():
    resp = "Hello from sms receipt printer"
    return resp

@app.route("/sms", methods=['POST'])
def sms_ahoy_reply():
    """Respond to incoming messages with a friendly SMS."""

    sms_info = get_sms_info(request)

    # debug print sms
    print(request.form)
    print("SMS ({}):\n {}".format(human_time(sms_info['timestamp']), sms_info['body']))

    # save sms into db
    save_sms(sms_info)

    # print sms on receipt printer
    # receiptPrintText("{}:\n    {}".format(human_time(sms_info['timestamp']), sms_info['body']))
    text = "{}:\n    {}".format(human_time(sms_info['timestamp']), sms_info['body'])
    tmp_receipt = generate_reciept.generate_image(text)
    receipt_printer.printImage(tmp_receipt)
    # time.sleep(1.5)
    # receipt_printer.spoolPaper(10)


    # Response is required (if message empty, it doesn't respond)
    resp = MessagingResponse()

    # Reply with a message on the first time:
    if find_by_number(sms_info['from'])[0] == 1:
        resp.message("Thanks for the input! Follow me on the internet: https://labseven.space")

    return str(resp)

if __name__ == "__main__":
    # create sms table
    conn = sqlite3.connect("sms_info.db")
    c = conn.cursor()
    c.execute('create table if not exists sms (id integer primary key, sms_from VARCHAR(15), sms_body VARCHAR, timestamp text)')
    conn.commit()
    conn.close()

    app.run(debug=True)
